//
//  FlutterWavePayment.swift
//  GoJekUser
//
//  Created by Shyamala's MacBook Pro on 09/09/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import RaveSDK
import UIKit

class FlutterWavePayment{
    static let shared = FlutterWavePayment()
    
    var transactionID : ((String)->())?
    
    func initiatePayment(vc : UIViewController,amount : String){
        let config = RaveConfig.sharedConfig()
        config.country = "NG" // Country Code
        config.currencyCode = "NGN" // Currency
        config.email = "customer@gmail.com"
        config.isStaging = true // Toggle this for staging and live environment
        config.phoneNumber = AppManager.share.getUserDetails()?.mobile ?? ""
        config.transcationRef = "ref" // transaction ref
        config.firstName = AppManager.share.getUserDetails()?.first_name ?? ""
        config.lastName = AppManager.share.getUserDetails()?.last_name ?? ""
        config.meta = [["metaname":"sdk", "metavalue":"ios"]]
          
        config.publicKey = APPConstant.FlutterWavePublich
        config.encryptionKey = APPConstant.FlutterSK

        let controller = NewRavePayViewController()
        let nav = UINavigationController(rootViewController: controller)
        controller.amount = amount
        controller.delegate = self
        vc.present(nav, animated: true)
    }
    
  
}

extension FlutterWavePayment : RavePayProtocol{
    func onDismiss() {
        self.onDismiss()
    }
    
    func tranasctionSuccessful(flwRef: String?, responseData: [String : Any]?) {
            print("responseDataresponseData",responseData)
        let id = responseData?["tx"] as? [String : Any] ?? ["":""]
        if (responseData?["id"] as? Int ?? 0) == 0{
            self.transactionID?("\(id["id"] as? Int ?? 0)")
        }else{
            self.transactionID?("\(responseData?["id"] as? Int ?? 0)")
        }
        
        print("txID",id["id"] as? Int ?? 0)
      
        }

        func tranasctionFailed(flwRef: String?, responseData: [String : Any]?) {
            print("tranasctionFailedtranasctionFailed",responseData)
        }

}
